﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class EmptySpaceException : Exception
    {

        public EmptySpaceException(string message) : base(message)
        {

        }

    }
}

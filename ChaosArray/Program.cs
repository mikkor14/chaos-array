﻿using System;

namespace ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {

            ChaosArr<String> arrString = new ChaosArr<String>(10);
            ChaosArr<int> arrInt = new ChaosArr<int>(10);
            ChaosArr<RandomElement> arrElement = new ChaosArr<RandomElement>(10);

            RandomElement el1 = new RandomElement(01);
            RandomElement el2 = new RandomElement(02);
            RandomElement el3 = new RandomElement(03);


            arrString.AddElement("Real");
            arrString.AddElement("G's");
            arrString.AddElement("move");
            arrString.AddElement("in");
            arrString.AddElement("silence");
            arrString.AddElement("like");
            arrString.AddElement("lasagna");

            arrString.Print();

            arrString.RetrieveElement();
            arrString.RetrieveElement();
            arrString.RetrieveElement();

            


            arrInt.AddElement(1);
            arrInt.AddElement(2);
            arrInt.AddElement(3);
            arrInt.AddElement(4);
            arrInt.AddElement(5);
            arrInt.AddElement(6);
            arrInt.AddElement(7);

            arrInt.Print();

            arrInt.RetrieveElement();
            arrInt.RetrieveElement();
            arrInt.RetrieveElement();




            arrElement.AddElement(el1);
            arrElement.AddElement(el2);
            arrElement.AddElement(el3);

            arrElement.Print();

            arrElement.RetrieveElement();
            arrElement.RetrieveElement();
            arrElement.RetrieveElement();




        }
    }
}

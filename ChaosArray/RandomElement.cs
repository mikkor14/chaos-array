﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class RandomElement
    {

        private int id;

        public RandomElement(int id)
        {
            this.Id = id;
        }

        public int Id { get => id; set => id = value; }
    }
}

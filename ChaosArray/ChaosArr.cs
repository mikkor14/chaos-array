﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class ChaosArr<T>
    {
        private int size;
        private T[] array;

        public ChaosArr(int size)
        {
            this.size = size;
            array = new T[size];
        }

        public void AddElement(T el)
        {
            Random random = new Random();
            int randomNumber = random.Next(0, size);

            if (size > 0)
            {
                try
                {
                    if (!EqualityComparer<T>.Default.Equals(array[randomNumber], default(T)))
                    {
                        throw new PlaceIsTakenException($"error: you are trying to insert | {el} | into an occupied space, please try again");

                    }
                    else
                    {
                        array[randomNumber] = el;
                        Console.WriteLine($"You inserted | {el} | into the array");
                    }
                }
                catch (PlaceIsTakenException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.WriteLine("The array is empty");
            }

        }

        public T RetrieveElement()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, size);
            T retrievedEl = default(T);
            if (size > 0)
            {
                try
                {
                    if(EqualityComparer<T>.Default.Equals(array[randomNumber], default(T)))
                    {
                        throw new EmptySpaceException("error: you are trying to retrieve from an empty space, please try again");
                    } else
                    {
                        retrievedEl = array[randomNumber];
                        Console.WriteLine($"You found | {retrievedEl} |");
                    }

                }
                catch (EmptySpaceException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                Console.WriteLine("The array is empty");
            }

            return retrievedEl;
        }


        public void Print()
        {

            foreach (T element in array)
            {
                if(element == null)
                {
                    Console.WriteLine("--------");
                } else
                {
                    Console.WriteLine(element);
                }
            }

        }

    }
}

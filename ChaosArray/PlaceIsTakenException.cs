﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChaosArray
{
    class PlaceIsTakenException : Exception
    {

        public PlaceIsTakenException(string message) : base(message)
        {
            
        }

    }
}
